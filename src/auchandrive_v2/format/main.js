"use strict";

const ctxt_pos = 0,
  ctxt_title = 1,
  ctxt_modal = 2,
  ctxt_prodlist = 3;

var sto = window.__sto,
  settings = require("./../../settings.json"),
  $ = window.jQuery,
  html = require("./main.html"),
  placeholder = settings.name,
  style = require("./main.css"),
  format = settings.format,
  target = settings.target == "" ? "_self" : settings.target,
  redirect = settings.redirect,
  cxt = window.__sto.tracker().value("rp"),
  helper_methods = sto.utils.retailerMethod,
  promise,
  viewTracking = false;

try {

  module.exports = {
    init: _init_()
  }

  function _init_() {
    promise = sto.utils.deferred().resolve();
    sto.load(format, function (tracker) {

      var container = document.createElement('article');

      var removers = {};
      removers[format] = function () {
        style.unuse();
        container.remove();
      };

      container.classList.add("sto-" + placeholder + "-banner");

      //Append format
        var firstBanner = document.querySelectorAll('.BANNER_3_ITEM_NOT_FULL__container>.BANNER_3_ITEM_NOT_FULL__link')[0];

        container.setAttribute("data-page", "home");
        container.classList.add("highco-slot");
        container.classList.add("BANNER_3_ITEM_NOT_FULL__link");
        var emplacement = firstBanner;
        emplacement.parentNode.insertBefore(container, emplacement);
        firstBanner.remove();


      // height mobile
      var heightDivMobile = function() {
        var heightDiv = document.querySelectorAll('.BANNER_3_ITEM_NOT_FULL__container>.BANNER_3_ITEM_NOT_FULL__link')[1].offsetHeight;
        console.log(heightDiv);
        if (window.matchMedia('(max-width: 767px)').matches) {
          container.style.height = heightDiv + "px";
        } else {
          container.style.height = "inherit";
        }
      }

      heightDivMobile();

      window.addEventListener('resize', function() {
        heightDivMobile();
      });



      style.use();
      tracker.display();

      document.querySelector('.sto-' + placeholder + '-banner').addEventListener('click', function (e) {
        e.preventDefault();
        tracker.click();
        window.open(redirect, target);
      });

      setInterval(function () {
        if (viewTracking === false) {
          formatInViewport(document.querySelector('.sto-' + placeholder + '-banner'), tracker);
        }
      }, 100);

      return removers;
    });
  }
} catch (e) {
  console.log(e);
}

function formatInViewport(el, tracker) {
  var top = el.offsetTop;
  var left = el.offsetLeft;
  var width = el.offsetWidth;
  var height = el.offsetHeight;
  while (el.offsetParent) {
    el = el.offsetParent;
    top += el.offsetTop;
    left += el.offsetLeft;
  }
  if (top >= window.pageYOffset && left >= window.pageXOffset && (top + height) <= (window.pageYOffset + window.innerHeight) && (left + width) <= (window.pageXOffset + window.innerWidth)) {
    tracker.view();
    viewTracking = true;
  }
};
