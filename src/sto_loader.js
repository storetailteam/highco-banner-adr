var settings = require("./settings.json");
settings.img_container = settings.img_container === "" ? "none" : "url(./../../img/"+settings.img_container+")";
settings.img_container_mob = settings.img_container_mob === "" ? "none" : "url(./../../img/"+settings.img_container_mob+")";

module.exports = function (source) {
    this.cacheable();
    var test = source
    .replace(/__PLACEHOLDER__/g, settings.name)
    .replace(/__CONTAINERIMG__/g,settings.img_container)
    .replace(/__CONTAINERIMGMOB__/g,settings.img_container_mob);
    return test;
};
