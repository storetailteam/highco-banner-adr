"use strict";
var sto = window.__sto,
    helper_methods = sto.utils.retailerMethod,
    settings = require("./../../settings"),
    $ = window.jQuery,
    gutter = require("./../gutter_hihgco/main.html"),
    placeholder = settings.name,
    styleg = require("./../gutter_hihgco/main.css"),
    html = require("./../format/main.html"),
    containercss = require("./../format/main.css"),
    redirect = settings.redirect,
    format = settings.format;




 module.exports={
     init : _init_(),
     styleGutter : styleg
 }

function _init_() {

    sto.load(format, function(tracker) {
            __sto.globals.highCo.promise.then(function() {

                if (tracker.value("up") === __sto.tracker().value("up")) {
                    tracker.display();
                    tracker.view();
                    containercss.use();
                    var container = $(html);
                    // addFooter($("#central-container .vignette-box").length, container);
                    $("#central-container").prepend(container);
                    $(".sto-"+placeholder+"-container").css("background-color", __CONTAINERBD__);
                    if (settings.redirect !== "") {
                        $(".sto-"+placeholder+"-container").css("cursor", "pointer");
                        $(".sto-"+placeholder+"-container").on('click', function(e) {
                            tracker.click();
                            if (settings.target == "") {
                                window.open(redirect,"_self");
                            } else if (settings.target !== "") {
                                window.open(redirect,settings.target);
                            }
                        });
                    }
                }
            });
        var removers = {};
        removers[format] = function() {
            styleg.unuse();
            $(".sto-"+placeholder+"-container").remove();
        };
        return removers;
    });
}
function addFooter(prodSize, container) {
    var zone = $("#crossZone-0"),
        temp_func;
        if (zone.length) {
            container.insertAfter("#crossZone-0");
            temp_func = window.goToAncreTo;
            window.goToAncreTo = function(nameAncre, taille) {
                temp_func(nameAncre, taille);
                if (nameAncre === "crossZone-0") {
                    container.insertAfter("#crossZone-1");
                }
            };
            return;
        }

        if (prodSize >= 8 && $(".banniereFH").length >= 1) {
            container.insertAfter(".vignette-box:nth-child(4)");
            $(".vignette-box:nth-child(10),.vignette-box:nth-child(11),.vignette-box:nth-child(12),.vignette-box:nth-child(13)").addClass("sto-Cto-search-space");
        } else if (prodSize >= 8) {
            container.insertAfter(".vignette-box:nth-child(5)");
            $(".vignette-box:nth-child(11),.vignette-box:nth-child(12),.vignette-box:nth-child(13),.vignette-box:nth-child(14)").addClass("sto-Cto-search-space");
        } else if (prodSize >= 8 && $(".miseEnAvant ").length >= 1) {
            container.insertAfter(".vignette-box:nth-child(4)");
            $(".vignette-box:nth-child(11),.vignette-box:nth-child(12),.vignette-box:nth-child(13),.vignette-box:nth-child(14)").addClass("sto-Cto-search-space");
        } else if (prodSize <= 7 && prodSize > 4) {
            container.insertAfter(".vignette-box:nth-child(4)");
            $(".vignette-box:nth-child(11),.vignette-box:nth-child(12),.vignette-box:nth-child(13),.vignette-box:nth-child(14)").addClass("sto-Cto-search-space");
        } else if (prodSize <= 4) {
            return false;
        }

    }
